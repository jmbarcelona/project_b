@extends('admin.layout.app')
@section('title', 'Dashboard')
@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{ $subordinate }}</h3>

                <p>Subordinates</p>
              </div>
              <div class="icon">
               <i class="fa fa-users"></i>
              </div>
              <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $superior }}</h3>

                <p>Superiors</p>
              </div>
              <div class="icon">
                <i class="fa fa-user-secret"></i>
              </div>
              <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{ $vip }}</h3>

                <p>VIP</p>
              </div>
              <div class="icon">
                <i class="fa fa-user-shield nav-icon "></i>
              </div>
              <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
            <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $vip }}</h3>

                <p>Pending Accounts</p>
              </div>
              <div class="icon">
                <i class="fa fa-user-shield nav-icon "></i>
              </div>
              <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-4">
              <div class="card">
                <div class="card-header"><h3><i class="fa fa-list-alt"></i> Today's Subordinates</h3></div>
                <div class="card-body">
                 <table class="table table-bordered dt-responsive nowrap" id="table_id" style="width: 100%;">
                   <thead>
                     <tr>
                       <th>LELE ID</th>
                       <th>Name</th>
                       <th class="text-center">Status</th>
                       <th class="text-center">Account Type</th>
                     </tr>
                   </thead>
                   <tbody>
                     @forelse($new_comers as $data)
                     <tr>
                       <td>{{ $data->lele_id }}</td>
                       <td>{{ ucwords($data->name) }}</td>
                       <td class="text-center">
                         @if($data->status == 1)
                          <span class="badge badge-success">Verified</span>
                         @else
                          <span class="badge badge-warning">Pending</span>
                         @endif
                       </td>
                       <td class="text-center">
                        @if($data->is_vip == 1)
                          <span class="badge badge-success">VIP</span>
                         @else
                          <span class="badge badge-default">Ordinary</span>
                         @endif
                      </td>
                     </tr>
                     @empty
                     <tr>
                       <td colspan="4" class="text-center">No Invites today.</td>
                     </tr>
                    @endforelse
                   </tbody>
                 </table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="card">
                <div class="card-header"><h3><i class="fa fa-hand-holding-usd"></i> Cashback Request</h3></div>
                <div class="card-body">
                  <table class="table table-bordered table-striped" id="tbl_users" style="width: 100%!important;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('page-modals')

@endpush

@push('js-scripts')

<script>
   
    var tbl_users;
show_users();
function show_users(){
  if(tbl_users){
    tbl_users.destroy();
  }
  tbl_samples = $('#tbl_users').DataTable({
    destroy: true,
    pageLength: 10,
    responsive: true,
    ajax: "{{ route('admin.users.vip.requests') }}",
    deferRender: true,
    columns: [
      {
        className: '',
        "data": 'lele_id',
        "title": 'Lele id',
      },
      {
        className: '',
        "data": 'name',
        "title": 'Name',
      },
      {
        className: '',
        "data": 'email_address',
        "title": 'Email address',
      },
      {
        className: '',
        "data": 'status',
        "title": 'Status',
      },
      {
        className: '',
        "data": 'gcash_name',
        "title": 'Gcash name',
      },
      {
        className: '',
        "data": 'contact_number',
        "title": 'Contact number',
      },
      {
        className: 'width-option-1 text-center',
        width: '15%',
        "data": 'id',
        "orderable": false,
        "title": 'Options',
        "render": function(data, type, row, meta){
          newdata = '';
          newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="check_documents('+row.id+')" type="button"><i class="fa fa-eye"></i></button> ';
          return newdata;
        }
      }
    ]
  });
}
</script>

@endpush
