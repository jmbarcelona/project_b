@extends('admin.layout.app')
@section('title', 'Pending Subordinates')
@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid pt-3">
        <div class="row mb-3">
          <div class="col-md-6 text-capitalize"><h1>Pending Subordinate Management</h2></div>
          <div class="col-md-12">
            <table class="table table-bordered table-striped" id="tbl_users" style="width: 100%!important;"></table>
          </div>
        </div>
      </div>
    </section>

@endsection

@push('page-modals')

@endpush

@push('js-scripts')

<script>   
var tbl_users;
show_users();
function show_users(){
  if(tbl_users){
    tbl_users.destroy();
  }
  tbl_samples = $('#tbl_users').DataTable({
    destroy: true,
    pageLength: 10,
    responsive: true,
    ajax: "{{ route('admin.users.list.subordinate.pending') }}",
    deferRender: true,
    columns: [
      {
        className: '',
        "data": 'lele_id',
        "title": 'Lele id',
      },
      {
        className: '',
        "data": 'name',
        "title": 'Name',
      },
      {
        className: '',
        "data": 'email_address',
        "title": 'Email address',
      },
      {
        className: '',
        "data": 'status',
        "title": 'Status',
      },
      {
        className: '',
        "data": 'gcash_name',
        "title": 'Gcash name',
      },
      {
        className: '',
        "data": 'contact_number',
        "title": 'Contact number',
      },
      {
        className: 'width-option-1 text-center',
        width: '15%',
        "data": 'id',
        "orderable": false,
        "title": 'Options',
        "render": function(data, type, row, meta){
          newdata = '';
          newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="approve_account('+row.id+');" type="button"><i class="fa fa-eye"></i></button>';
          return newdata;
        }
      }
    ]
  });
}

  
function delete_company(id){
  var url = '{{ route('admin.users.account.approve') }}';
  swal({
        title: "Are you sure?",
        text: "Do you want to delete ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
        function(){
         $.ajax({
          type:"GET",
          url:url,
          data:{},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              swal("Success", response.message, "success");
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
      });
  }

</script>

@endpush
