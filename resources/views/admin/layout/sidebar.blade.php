 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.home') }}" class="brand-link">
      <img src="{{ asset('logo.png') }}" alt="Company Logo" class="brand-image">
      <span class="brand-text font-weight-bold">Lele Gold</span>
    </a>

    <style>
      #avatar_sidebar{
               width: 2rem;
               height: 2rem;
             }
    </style>
    
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
              <img src="{{ asset('dist/img/avatar_default.png') }}" class="img-circle elevation-2" alt="Employee Male Default">
          
        </div>
        <div class="info">
          <a href="" class="d-block"><b>#{{ auth()->user()->lele_id }}</b>{{ auth()->user()->getFullName()  }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="{{ route('admin.home') }}" class="nav-link {{ (request()->is('admin'))? 'active': '' }}">
                <i class="fas fa-chart-bar"></i>
                <p>Dashboard</p>
              </a>
            </li> 
        </ul>

        @if(auth()->user()->user_type === 1)
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="{{ route('admin.users.superior') }}" class="nav-link {{ (request()->is('admin/users/superior'))? 'active': '' }}">
                <i class="fas fa-user-secret"></i>
                <p>Superior</p>
              </a>
            </li> 
        </ul>
        @endif
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="{{ route('admin.users.index') }}" class="nav-link {{ (request()->is('admin/users'))? 'active': '' }}">
                <i class="fas fa-users"></i>
                <p>Subordinate</p>
              </a>
            </li> 
        </ul>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="{{ route('admin.users.subordinate.pending') }}" class="nav-link {{ (request()->is('admin/users/subordinate-pending'))? 'active': '' }}">
                <i class="fas fa-user-lock"></i>
                <p>Pending Subordinate</p>
                <span class="badge badge-danger navbar-badge">3</span>
              </a>
            </li> 
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>