<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title> @yield('title')</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('logo.png') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-new/datatables.min.css') }}"/>

    <!-- Lada Button -->
    <link type="text/css" href="{{ asset('plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
  
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">

    <!-- Custom style -->
    <link rel="stylesheet" href="{{ asset('style.css') }}">

    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">



    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    @stack('styles')    

    <!-- CUSTOM CSS FILES HERE -->

</head>
    
<body class="hold-transition sidebar-mini sidebar-collapse">

<div class="wrapper">

    @include('admin.layout.navbar')

    @include('admin.layout.sidebar')


    <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">

        @include('admin.layout.breadcrumb')

        @yield('content')

      </div>

    <!-- /.content-wrapper -->

    @include('admin.layout.footer')

</div>

<!-- Global Modals -->

@stack('page-modals')

<!-- End Global Modals -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- ladda -->
<script src="{{ asset('plugins/ladda/spin.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.jquery.min.js') }}"></script>

<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script src="{{ asset('plugins/datatables-new/datatables.min.js') }}"></script>

<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('js/csrf-ajax.js') }}"></script>

<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/validation.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

@stack('js-scripts') 



</body>
</html>
