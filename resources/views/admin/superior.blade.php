@extends('admin.layout.app')
@section('title', 'Superiors')
@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid pt-3">
        <div class="row mb-3">
          <div class="col-md-6 text-capitalize"><h1>Superior Management</h2></div>
          <div class="col-md-6 text-right"><button type="button" onclick="add_users();" class="btn btn-primary">Add Superior</button></div>
          <div class="col-md-12">
            <table class="table table-bordered table-striped" id="tbl_users" style="width: 100%!important;"></table>
          </div>
        </div>
      </div>
    </section>

    <form class="needs-validation" id="user_form" action="{{ route('admin.users.save') }}" novalidate>
      <div class="modal fade" tabindex="-1" role="dialog" id="modal_user_form" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal_user_form_title">Add Superior</h5>
              <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <input type="hidden" name="id" id="id" />
                <input type="hidden" name="user_type" id="user_type" value="2" />
                <div class="col-md-12">
                  <label>Lele id</label>
                  <input type="text" name="lele_id" id="lele_id" class="form-control " required  placeholder="Enter lele id"/>
                </div>
                <div class="col-md-12">
                  <label>Name</label>
                  <input type="text" name="name" id="name" class="form-control " required  placeholder="Enter name"/>
                </div>
                <div class="col-md-12">
                  <label>Email address</label>
                  <input type="email" name="email_address" id="email_address" class="form-control "   placeholder="Enter email address"/>
                </div>
                <div class="col-md-12">
                  <label>Status</label>
                  <select name="status" id="status" class="form-control " required>
                    <option value="1">Active</option>
                    <option value="0">Pending</option>
                    <option value="2">Ban</option>
                  </select>
                </div>
                <div class="col-md-12">
                  <label>Gcash name</label>
                  <input type="text" name="gcash_name" id="gcash_name" class="form-control "   placeholder="Enter gcash name"/>
                </div>
                <div class="col-md-12">
                  <label>Contact number</label>
                  <input type="text" name="contact_number" id="contact_number" class="form-control "   placeholder="Enter contact number"/>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-success" id="user_form_btn" >Save</button>
            </div>
          </div>
        </div>
      </div>
    </form>
    <!-- /.content -->

@endsection

@push('page-modals')

@endpush

@push('js-scripts')

<script>   
var tbl_users;
show_users();
function show_users(){
  if(tbl_users){
    tbl_users.destroy();
  }
  tbl_samples = $('#tbl_users').DataTable({
    destroy: true,
    pageLength: 10,
    responsive: true,
    ajax: "{{ route('admin.users.list.superior') }}",
    deferRender: true,
    columns: [
      {
        className: '',
        "data": 'lele_id',
        "title": 'Lele id',
      },
      {
        className: '',
        "data": 'name',
        "title": 'Name',
      },
      {
        className: '',
        "data": 'email_address',
        "title": 'Email address',
      },
      {
        className: '',
        "data": 'status',
        "title": 'Status',
      },
      {
        className: '',
        "data": 'gcash_name',
        "title": 'Gcash name',
      },
      {
        className: '',
        "data": 'contact_number',
        "title": 'Contact number',
      },
      {
        className: 'width-option-1 text-center',
        width: '15%',
        "data": 'id',
        "orderable": false,
        "title": 'Options',
        "render": function(data, type, row, meta){
          newdata = '';
          newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_users('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
          newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_users('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
          return newdata;
        }
      }
    ]
  });
}


$("#user_form").on('submit', function(e){
  e.preventDefault();
  let id = $('#id').val();
  let url = $(this).attr('action');
  let formData = $(this).serialize();
  $.ajax({
    type:"POST",
    url:url+'/'+id,
    data:formData,
    dataType:'json',
    beforeSend:function(){
      $('#user_form_btn').prop('disabled', true);
    },
    success:function(response){
      // console.log(response);
      if (response.status == true) {
        swal("Success", response.message, "success");
        show_users();
        $('#modal_user_form').modal('hide');
      }else{
        console.log(response);
      }
        validation('user_form', response.error);
        $('#user_form_btn').prop('disabled', false);
    },
    error: function(error){
      $('#user_form_btn').prop('disabled', false);
      console.log(error);
    }
  });
});

function add_users(){
  $("#id").val('');
  $('#lele_id').val('');
  $('#name').val('');
  $('#email_address').val('');
  $('#status').val('');
  $('#gcash_name').val('');
  $('#contact_number').val('');
  $("#modal_user_form").modal('show');
}


function edit_users(id){
  $.ajax({
    type:"GET",
    url:"{{ route('admin.users.find') }}/"+id,
    data:{},
    dataType:'json',
    beforeSend:function(){
    },
    success:function(response){
      // console.log(response);
      if (response.status == true) {
        $('#id').val(response.data.id);
        $('#lele_id').val(response.data.lele_id);
        $('#name').val(response.data.name);
        $('#email_address').val(response.data.email_address);
        $('#status').val(response.data.status);
        $('#gcash_name').val(response.data.gcash_name);
        $('#contact_number').val(response.data.contact_number);
        $('#modal_user_form').modal('show');
      }else{
        console.log(response);
      }
    },
    error: function(error){
      console.log(error);
    }
  });
}


function delete_users(id){
  swal({
    title: "Are you sure?",
    text: "Do you want to delete users?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
  },
  function(){
    $.ajax({
      type:"DELETE",
      url:"{{ route('admin.users.delete') }}/"+id,
      data:{},
      dataType:'json',
      beforeSend:function(){
    },
    success:function(response){
      // console.log(response);
      if (response.status == true) {
        show_users();
        swal("Success", response.message, "success");
      }else{
        console.log(response);
      }
    },
    error: function(error){
      console.log(error);
    }
    });
  });
}

</script>

@endpush
