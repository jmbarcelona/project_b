<?php 
    $link = "http://proxy.abcfishinggames.com:8180/apk_proxy/apk/download.html?accountId=3160397";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LELE Gold</title>

    <link rel="shortcut icon" href="images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/style.css') }}">
</head>

<body>

    <!-- start preloader -->
    <div class="preloader" id="preloader"></div>
    <!-- end preloader -->

    <a href="#" class="scrollToTop"><i class="fas fa-angle-double-up"></i></a>

    <!-- header-section start -->
     <header id="header-section">
        <div class="overlay">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="logo-section flex-grow-1 d-flex align-items-center">
                        <a class="site-logo site-title" href="{{ route('website') }}?#banner-section"><img src="{{ asset('logo.png') }}" width="65"  
                                alt="site-logo"></a>
                    </div>
                    <button class="navbar-toggler ml-auto collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>
                    <nav class="navbar navbar-expand-lg p-0">
                        <div class="navbar-collapse collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav main-menu ml-auto">
                                <li><a href="{{ route('website') }}?#banner-section">Home</a></li>
                                <li><a href="{{ route('website') }}?#how-works-section">How to Join?</a></li>
                                <li><a href="{{ route('website') }}?#features-section">Features</a></li>
                                <li><a href="#" onclick="modal_disclaimer();">Disclaimer</a></li>
                                <li><a href="#contact-section">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                      <div class="right-area header-action d-flex align-items-center">
                        <a href="{{ route('login') }}" class="cmn-btn">Login</a>
                        <a href="{{ route('join') }}" class="cmn-btn">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-section end -->

    <!-- banner-section start -->
    <section id="banner-section" class="inner-banner profile features shop">
        <div class="ani-img">
            <img class="img-1" src="images/banner-circle-1.png" alt="icon">
            <img class="img-2" src="images/banner-circle-2.png" alt="icon">
            <img class="img-3" src="images/banner-circle-2.png" alt="icon">
        </div>
        <div class="banner-content d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="main-content">
                            <h1>Contact Us</h1>
                            <p>We're here to support your concerns.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

    <!-- Contact Start -->
    <section id="contact-section" class="pt-5 pb-120">
        <div class="overlay">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <form action="#" method="post">
                            <h5>Leave your message or suggestions</h5>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" placeholder="Enter your Name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" placeholder="Enter your email">
                            </div>
                            <div class="form-group">
                                <label for="email">Message</label>
                                <textarea rows="6" placeholder="Enter your message"></textarea>
                            </div>
                            <button class="cmn-btn" type="submit">Submit Now</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact End -->

    <!-- footer-section start -->
    <footer id="footer-section">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-top">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 col-md-8">
                                    <div class="top-area text-center">
                                        <h3>JOIN NOW</h3>
                                        <p>RECEIVE CASHBACK AND SPECIAL REWARDS.</p>
                                    </div>
                                    <div class="btn-play d-flex justify-content-center align-items-center text-center">
                                        <a href="registration.html" class="cmn-btn" style="height: 80px !important; width:300px; padding: 20px; font-size:30px;">Download Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-mid pt-120">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-lg-8 col-md-8 d-flex justify-content-md-between justify-content-center align-items-center cus-grid">
                        <div class="logo-section">
                            <a class="site-logo site-title" href="{{ route('website') }}?#banner-section"><img src="{{ asset('logo.png') }}" alt="site-logo"></a>
                        </div>
                        <ul class="menu-side d-flex align-items-center">
                            <li><a href="{{ route('website') }}?#banner-section" class="active">Home</a></li>
                            <li><a href="{{ route('website') }}?#features-section">Features</a></li>
                            <li><a href="{{ route('contact_us') }}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 d-flex align-items-center justify-content-center justify-content-md-end">
                         <div class="right-area">
                            <ul class="d-flex">
                                <li><a href="https://www.facebook.com/groups/3056137081198675" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="{{ $link }}" target="_blank"><i class="fab fa-android"></i></a></li>
                                <li><a href="{{ $link }}" target="_blank"><i class="fab fa-apple"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="main-content">
                    <div class="row d-flex align-items-center justify-content-center">
                        <div class="col-lg-12 col-md-6">
                            <div class="left-area text-center">
                                <p>Copyright © {{ date('Y') }}. All Rights Reserved By
                                    <a href="#">Team Bulitas</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer-section end -->
    @include('modal_disclaimer')


    <script src="{{ asset('website/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('website/js/slick.js') }}"></script>
    <script src="{{ asset('website/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('website/js/fontawesome.js') }}"></script>
    <script src="{{ asset('website/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('website/js/wow.js') }}"></script>
    <script src="{{ asset('website/js/main.js') }}"></script>
    @include('script_disclaimer')

</body>

</html>