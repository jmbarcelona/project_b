 <div class="modal fade" role="dialog" id="age_modal" style="background: transparent; backdrop-filter: blur(10px);">
      <div class="modal-dialog">
        <div class="modal-content" style="background: #201678 !important;">                
          <div class="modal-body text-center">
             <h5 class="mt-4">Are you age 20 or above?</h5>
             <br>
              <div class="btn-play d-flex justify-content-center align-items-center text-center">
                <a href="#javascript:;" onclick="window.location = 'https://www.youtube.com/watch?v=e_04ZrNroTo'" class="cmn-btn mr-3">No</a>
                <a href="#javascript:;" onclick="modal_disclaimer();" class="cmn-btn">Yes, I am</a>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- footer-section end -->
 <div class="modal fade" role="dialog" id="modal_disclaimer" style="background: transparent; backdrop-filter: blur(10px);">
      <div class="modal-dialog">
        <div class="modal-content" style="background: #201678 !important;">
          <div class="modal-header" style="border:none!important;">
            <div class="modal-title">
            <h3>Disclaimer</h3>
            </div>
          </div>
          <div class="modal-body">
            <h4>Lele Gold</h4>
            <p>is a play to earn games.</p>
            <br>
            <p>We are not forcing anyone to invest or download this game. You can play and earn with this game without investing.</p>
            <br>
            <h4><b style="color:red;">Do your own Research</b></h4>
            <p>Please research before investing and put some money into this game.</p>
            <br>
            <h4><b style="color:red;">Take your own risk</b></h4>
            <p>The game is meant to play and to have fun. And if you spend or put some money into this do it at your own risk.</p>
            <br><br>
            <div class="btn-play d-flex justify-content-center align-items-center text-center">
                <a href="#javascript:;" data-dismiss="modal" onclick="underestand();" class="cmn-btn" style="height: 80px !important; width:300px; padding: 20px; font-size:30px;">I Understand</a>
            </div>
          </div>
        </div>
      </div>
    </div>