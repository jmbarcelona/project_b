<x-mail::message>
<b style="color: #333 !important;">Hi {{ ucwords($data['name']) }}</b>,

<h3 style="color: #333 !important;">Your account has been verified by our support you can login now.</h3>
<p>{{ route('login') }}</p>


Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
