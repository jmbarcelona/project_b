<x-mail::message>
<b style="color: #333 !important;">Hi {{ ucwords($data['name']) }}</b>,

<h3 style="color: #333 !important;">Here is your Account Details.</h3>

<p style="margin:5px; color: #333 !important;"><b>Email Address</b>: {{ $data['email'] }}</p>
<p style="margin:5px; color: #333 !important;"><b>Password</b>: {{ $data['password'] }}</p>
<br>
<p style="color: #333 !important;">Do not share your account details to anyone.</p>
<?php $link = $data['token'] ?>
<x-mail::button :url="'{{ $link }}'">
Validate Your Account
</x-mail::button>
<p style="color: #333 !important;">If you can't click the button copy and paste this Link</p>
<p>{{ $link }}</p>


Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
