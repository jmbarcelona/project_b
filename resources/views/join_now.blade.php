<?php 
    $link = "http://proxy.abcfishinggames.com:8180/apk_proxy/apk/download.html?accountId=3160397";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Registration</title>

    <link rel="shortcut icon" href="images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">

</head>

<body>

    <!-- start preloader -->
    <div class="preloader" id="preloader"></div>
    <!-- end preloader -->

    <a href="#" class="scrollToTop"><i class="fas fa-angle-double-up"></i></a>

    <!-- header-section start -->
     <header id="header-section">
        <div class="overlay">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="logo-section flex-grow-1 d-flex align-items-center">
                        <a class="site-logo site-title" href="{{ route('website') }}?#banner-section"><img src="{{ asset('logo.png') }}" width="65" 
                                alt="site-logo"></a>
                    </div>
                    <button class="navbar-toggler ml-auto collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>
                    <nav class="navbar navbar-expand-lg p-0">
                        <div class="navbar-collapse collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav main-menu ml-auto">
                                <li><a href="{{ route('website') }}?#banner-section">Home</a></li>
                                <li><a href="{{ route('website') }}?#how-works-section">How to Join?</a></li>
                                <li><a href="{{ route('website') }}?#features-section">Features</a></li>
                                <li><a href="#" onclick="modal_disclaimer();">Disclaimer</a></li>
                                <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                      <div class="right-area header-action d-flex align-items-center">
                        <a href="{{ route('login') }}" class="cmn-btn">Login</a>
                        <a href="{{ route('join') }}" class="cmn-btn">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-section end -->

    <!-- Contact Start -->
    <section id="contact-section">
        <div class="overlay">
            <div class="container">
                <div class="row justify-content-center pt-120 pb-120" style="margin-top: 110px;">
                    <div class="col-md-6">
                        <h3>Instructions.</h3>
                        <p>Please follow all the Instructions first before register your account.</p>
                        
                        <p class="mt-3">1. Download and Install the APP.</p>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="javascript:;" onclick="window.open('{{ $link }}','_blank','width=600,height=1000,top=0,left=1500')" class="cmn-btn text-white" type="submit"><img src="{{ asset('website/images/android.png') }}" width="20"> Download</a>
                                <a href="javascript:;" onclick="window.open('{{ $link }}','_blank','width=600,height=1000,top=0,left=1500')" class="cmn-btn text-white" type="submit"><img src="{{ asset('website/images/ios.png') }}" width="20"> Download</a>

                            </div>
                        </div>
                        <p class="mt-3">2. Login via Facebook Or Mobile.</p>
                        <a href="{{ asset('info/1.png') }}" target="_blank"><img src="{{ asset('info/1.png') }}" alt="" class="img-fluid"></a>
                        <p class="mt-3">3. On the homepage click this menu.</p>
                        <a href="{{ asset('info/2.png') }}" target="_blank"><img src="{{ asset('info/2.png') }}" alt="" class="img-fluid"></a>
                        <p class="mt-3">3. Get your <b>LELE GOLD Account ID</b> use this for your registraion.</p>
                        <p class="mt-3">NOTE: Make sure that your superior ID is <b>3160397</b>.</p>
                        <a href="{{ asset('info/3.png') }}" target="_blank"><img src="{{ asset('info/3.png') }}" alt="" class="img-fluid"></a>
                        <p class="mt-3">4. Use your Email address and <b>LELE GOLD Account ID</b> to register.</p>
                        <a href="{{ asset('info/5.png') }}" target="_blank" class="mb-5"><img src="{{ asset('info/5.png') }}" alt="" class="img-fluid"></a>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5 ">
                        <form id="user_form" class="needs-validation" action="{{ route('auth.register') }}" method="post">
                            <h5>Register Your Account Here.</h5>
                             <input type="hidden" name="g-token" id="g-token">
                            <input type="hidden" style="opacity: 0;" name="ref_id" id="ref_id" value="{{ $invite_code }}">
                            <div class="form-group">
                                <label for="email_address">Name</label>
                                <input type="text" id="name" name="name" placeholder="Enter your Name" class="form-control text-capitalize">
                            </div>
                            <div class="form-group">
                                <label for="email_address">Email Address</label>
                                <input type="text" id="email_address" name="email_address" placeholder="Enter your Name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="lele_id">Lele Gold ID</label>
                                <input type="text" id="lele_id" name="lele_id" maxlength="10" placeholder="Enter Lele Gold Account ID" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="confirm_lele_id">Confirm ID</label>
                                <input type="text" id="confirm_lele_id" maxlength="10" name="confirm_lele_id" placeholder="Confirm Lele Gold Account ID" class="form-control">
                            </div>
                            <div class="text-right">
                                <button class="cmn-btn" id="submit" type="submit">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact End -->

    @include('modal_disclaimer')
    <script src="{{ asset('website/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('website/js/slick.js') }}"></script>
    <script src="{{ asset('website/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('website/js/fontawesome.js') }}"></script>
    <script src="{{ asset('website/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('website/js/wow.js') }}"></script>
    <script src="{{ asset('website/js/main.js') }}"></script>
    <script src="{{ asset('js/csrf-ajax.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6LfHJvkjAAAAABZdHFU_9n-YdGn2u8uwGsFSqZfY"></script>
    @include('script_disclaimer')

    <script type="text/javascript">
        get_new_recapcha();

        // Token from capthca
        function get_new_recapcha(){
         grecaptcha.ready(function() {
              grecaptcha.execute('6LfHJvkjAAAAABZdHFU_9n-YdGn2u8uwGsFSqZfY', {action: 'contact_form'}).then(function(token) {
                  $("#g-token").val(token);
              });
            });
        }

        $("#user_form").on('submit', function(e){
            e.preventDefault();
            let url = $(this).attr('action');
            let formData = $(this).serialize();
            $.ajax({
                type:"POST",
                url:url,
                data:formData,
                dataType:'json',
                beforeSend:function(){
                    $('#submit').prop('disabled', true);
                    $('#submit').text('Please Wait...');
                },
                success:function(response){
                    // console.log(response);
                    if (response.status == true) {
                        swal("Thank you!", response.message, "success");
                        get_new_recapcha();
                        $("#name").val('');
                        $("#email_address").val('');
                        $("#lele_id").val('');
                        $("#confirm_lele_id").val('');
                    }else{
                        swal("Error", response.message, "error");
                        get_new_recapcha();
                    }
                        validation('user_form', response.error);
                        $('#user_form_btn').prop('disabled', false);
                    $('#submit').prop('disabled', false);
                    $('#submit').text('Register');

                },
                error: function(error){
                    $('#submit').prop('disabled', false); 
                    $('#submit').text('Register');                   
                    swal("Error", "Please try again later.", "error");
                }
            });
        });

    </script>
</body>

</html>