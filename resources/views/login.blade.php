<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">

</head>

<body>

    <!-- start preloader -->
    <div class="preloader" id="preloader"></div>
    <!-- end preloader -->

    <a href="#" class="scrollToTop"><i class="fas fa-angle-double-up"></i></a>

    <!-- header-section start -->
     <header id="header-section">
        <div class="overlay">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="logo-section flex-grow-1 d-flex align-items-center">
                        <a class="site-logo site-title" href="{{ route('website') }}?#banner-section"><img src="{{ asset('logo.png') }}" width="65" 
                                alt="site-logo"></a>
                    </div>
                    <button class="navbar-toggler ml-auto collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>
                    <nav class="navbar navbar-expand-lg p-0">
                        <div class="navbar-collapse collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav main-menu ml-auto">
                                <li><a href="{{ route('website') }}?#banner-section">Home</a></li>
                                <li><a href="{{ route('website') }}?#how-works-section">How to Join?</a></li>
                                <li><a href="{{ route('website') }}?#features-section">Features</a></li>
                                <li><a href="#" onclick="modal_disclaimer();">Disclaimer</a></li>
                                <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                      <div class="right-area header-action d-flex align-items-center">
                        <a href="{{ route('login') }}" class="cmn-btn">Login</a>
                        <a href="{{ route('join') }}" class="cmn-btn">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-section end -->

    <!-- Contact Start -->
    <section id="contact-section" class="pt-120 pb-120" style="margin-top: 120px;">
        <div class="overlay">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <form id="login_form" action="{{ route('auth.login') }}" class="needs-validation" method="post">
                            <h5>Login your account.</h5>
                             <input type="hidden" name="g-token" id="g-token">
                            <div class="form-group">
                                <label for="contact_number">Email Address</label>
                                <input type="email" id="email_address" name="email_address" class="form-control" placeholder="Enter your Email Address">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password" class="form-control" placeholder="Enter your email">
                            </div>
                            <div class="text-right">
                                <button class="cmn-btn" type="button" onclick="window.location ='{{ route('join') }}'">Sign Up</button>
                                <button class="cmn-btn" type="submit" id="submit">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact End -->

    @include('modal_disclaimer')



    <script src="{{ asset('website/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('website/js/slick.js') }}"></script>
    <script src="{{ asset('website/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('website/js/fontawesome.js') }}"></script>
    <script src="{{ asset('website/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('website/js/wow.js') }}"></script>
    <script src="{{ asset('website/js/main.js') }}"></script>
    <script src="{{ asset('js/csrf-ajax.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6LfHJvkjAAAAABZdHFU_9n-YdGn2u8uwGsFSqZfY"></script>

    @include('script_disclaimer')

    <script type="text/javascript">
         get_new_recapcha();

        // Token from capthca
        function get_new_recapcha(){
         grecaptcha.ready(function() {
              grecaptcha.execute('6LfHJvkjAAAAABZdHFU_9n-YdGn2u8uwGsFSqZfY', {action: 'contact_form'}).then(function(token) {
                  $("#g-token").val(token);
              });
            });
        }
        $("#login_form").on('submit', function(e){
            e.preventDefault();
            let url = $(this).attr('action');
            let formData = $(this).serialize();
            $.ajax({
                type:"POST",
                url:url,
                data:formData,
                dataType:'json',
                beforeSend:function(){
                    $('#submit').prop('disabled', true);
                    $('#submit').text('Please Wait...');
                },
                success:function(response){
                    // console.log(response);
                    if (response.status == true) {
                        // console.log(response);    
                        window.location= response.redirect;                    
                    }else{
                        swal("Error", response.message, "error");
                        get_new_recapcha();
                        console.log(response);
                    }
                        validation('login_form', response.error);
                        $('#login_form_btn').prop('disabled', false);
                    $('#submit').prop('disabled', false);
                    $('#submit').text('Login');

                },
                error: function(error){
                    $('#submit').prop('disabled', false); 
                    $('#submit').text('Login');                   
                    console.log(error);
                }
            });
        });
    </script>

</body>

</html>