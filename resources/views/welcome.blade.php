<?php 
    $link = "http://proxy.abcfishinggames.com:8180/apk_proxy/apk/download.html?accountId=3160397";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LELE Gold</title>

    <link rel="shortcut icon" href="images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/style.css') }}">
</head>

<body>

    <!-- start preloader -->
    <div class="preloader" id="preloader"></div>
    <!-- end preloader -->

    <a href="#" class="scrollToTop"><i class="fas fa-angle-double-up"></i></a>

    <!-- header-section start -->
     <header id="header-section">
        <div class="overlay">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="logo-section flex-grow-1 d-flex align-items-center">
                        <a class="site-logo site-title" href="{{ route('website') }}?#banner-section"><img src="{{ asset('logo.png') }}" width="65" 
                                alt="site-logo"></a>
                    </div>
                    <button class="navbar-toggler ml-auto collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>
                    <nav class="navbar navbar-expand-lg p-0">
                        <div class="navbar-collapse collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav main-menu ml-auto">
                                <li><a href="#banner-section">Home</a></li>
                                <li><a href="#how-works-section">VIP Subordinate</a></li>
                                <li><a href="#features-section">Features</a></li>
                                <li><a href="#" onclick="modal_disclaimer();">Disclaimer</a></li>
                                <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                      <div class="right-area header-action d-flex align-items-center">
                        <a href="{{ route('login') }}" class="cmn-btn">Login</a>
                        <a href="{{ route('join') }}" class="cmn-btn">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-section end -->

    <!-- banner-section start -->
    <section id="banner-section">
        <div class="banner-content d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center m-3">
                    <div class="col-lg-12">
                        <div class="main-content">
                            <div class="row justify-content-center">
                                <div class="col-lg-12">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="bottom-area text-center">
                                                <img src="{{ asset('logo2.png') }}" width="320" alt="banner-vs">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="top-area justify-content-center text-center">
                                <h3 style="color: white; text-shadow: 1px 1px 0px #000;" >BE MY VIP SUBORDINATE</h3>
                                <h4 style="text-shadow: 2px 2px 0px #333;">AND GET <b>₱100</b> <cash style="color:red;">CASHBACK</cash></h4>
                                <h4 style="text-shadow: 2px 2px 0px #333;">AND MORE PRICES</h4>
                                <div class="btn-play d-flex justify-content-center align-items-center text-center">
                                    <a href="{{ route('join') }}" class="cmn-btn" style="height: 80px !important; width:300px; padding: 20px; font-size:30px;">Download Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ani-illu">
                    <img class="left-1 wow fadeInUp" src="{{ asset('website/images/right-banner.png') }}" alt="image">
                    <img class="right-2 wow fadeInUp" src="{{ asset('website/images/left-banner.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

    <!-- Available Game In start -->
    <section id="available-game-section">
        <div class="overlay pb-120">
            <div class="container wow fadeInUp">
                <div class="main-container">
                    <div class="row justify-content-between">
                        <div class="col-lg-10">
                            <div class="section-header">
                                <h2 class="title">Available Games</h2>
                                <p>Lele Gold have a lot of exciting games</p>
                            </div>
                        </div>
                    </div>
                    <div class="available-game-carousel">
                        <div class="single-item">
                            <a href="#"><img src="{{ asset('website/images/game-3.png') }}" alt="image"></a>
                        </div>
                        <div class="single-item">
                            <a href="#"><img src="{{ asset('website/images/game-4.png') }}" alt="image"></a>
                        </div>
                        <div class="single-item">
                            <a href="#"><img src="{{ asset('website/images/game-2.png') }}" alt="image"></a>
                        </div>
                        <div class="single-item">
                            <a href="#"><img src="{{ asset('website/images/game-1.png') }}" alt="image"></a>
                        </div>
                        <div class="single-item">
                            <a href="#"><img src="{{ asset('website/images/game-5.png') }}" alt="image"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Available Game In end -->

     <!-- How Works start -->
    <section id="how-works-section" class="border-area">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-header">
                            <h2 class="title">How to Join VIP SUBORDINATE?</h2>
                            <p>It's easier than you think. Follow 4 simple easy steps</p>
                        </div>
                    </div>
                </div>
                <div class="row mp-top">
                    <div class="col-lg-3 col-md-3 col-sm-6 d-flex justify-content-center">
                        <div class="single-item">
                            <div class="icon-area">
                                <span>1</span>
                                <img src="{{ asset('website/images/how-icon-1.png') }}" alt="image">
                            </div>
                            <div class="text-area">
                                <h5>Download & Install</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 d-flex justify-content-center obj-rel">
                        <div class="single-item">
                            <div class="icon-area">
                                <span>2</span>
                                <img src="{{ asset('website/images/how-icon-3.png') }}" alt="image">
                            </div>
                            <div class="text-area">
                                <h5>SIGN UP AND SIGN IN</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 d-flex justify-content-center obj-alt">
                        <div class="single-item">
                            <div class="icon-area">
                                <span>3</span>
                                <img src="{{ asset('website/images/format-icon-5.png') }}" width="40" alt="image">
                            </div>
                            <div class="text-area">
                                <h5>Avail THE VIP 1</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 d-flex justify-content-center obj-rel">
                        <div class="single-item">
                            <div class="icon-area">
                                <span>4</span>
                                <img src="{{ asset('website/images/how-icon-4.png') }}" alt="image">
                            </div>
                            <div class="text-area">
                                <h5>Get ₱100 CASHBACK</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center pt-5">
                    <div class="col-lg-6 text-center">
                        <h4 class="title">Enjoy The Game</h4>
                        <p>Earn Prices and Special Rewards</p>
                         <div class="btn-play d-flex justify-content-center align-items-center text-center">
                            <a href="{{ route('join') }}" class="cmn-btn" style="height: 80px !important; width:800px; padding: 20px; font-size:30px;">Sign Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- How Works end -->
    
    <!-- Features In start -->
    <section id="features-section">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-header text-center">
                            <h2 class="title">VIP Subordinate Features</h2>
                            <p>We want to grow your earnings with this special features.</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center pm-none">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <div class="img-area">
                                <img src="{{ asset('website/images/features-icon-2.png') }}" alt="image">
                            </div>
                            <h5>Instant Cashback</h5>
                            <p>Avail VIP 1/Farm Package and get ₱100 Cashback.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <div class="img-area">
                                <img src="{{ asset('website/images/features-icon-1.png') }}" alt="image">
                            </div>
                            <h5>MEMBER REFERAL Program</h5>
                            <p>Invite 10 members with VIP 1 and get ₱500 rewards.</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center pm-none mt-5">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <div class="img-area">
                                <img src="{{ asset('website/images/features-icon-4.png') }}" alt="image">
                            </div>
                            <h5>Farm Competition</h5>
                            <h5 class="m-0">
                                <small>coming soon!</small>
                            </h5>
                            <p>Best Farm design competition can win <br>five thousand pesos cash.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <div class="img-area">
                                <img src="{{ asset('website/images/how-icon-4.png') }}" alt="image">
                            </div>
                            <h5>exclusive redeem code</h5>
                            <h5 class="m-0">
                                <small>coming soon!</small>
                            </h5>
                            <p>Each member will get redeem code on their accounts exclusively.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <div class="img-area">
                                <img src="{{ asset('website/images/features-icon-5.png') }}" alt="image">
                            </div>
                            <h5>raffle draws</h5>
                            <h5 class="m-0">
                                <small>coming soon!</small>
                            </h5>
                            <p>Monthly draw to our VIP subordinates and have a chance to win ten thousand pesos cash.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Features In end -->

        <!-- Players of the Week In start -->
   <!--  <section id="counter-section">
        <div class="overlay pt-120 pb-120">
        
        </div>
    </section> -->
    <!-- Players of the Week In end -->

    <!-- Testimonials In start -->
    <section id="testimonials-section">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-header text-center">
                            <h2 class="title">Our Subordinate Reviews</h2>
                            <p>Thank You for all your support. keep Grinding!</p>
                        </div>
                    </div>
                </div>
                <div class="row mp-none">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <p>I play Tournament every day, it's a great way to relax and win cash too!</p>
                            <div class="bottom-area d-flex justify-content-between">
                                <div class="left-area d-flex">
                                    <div class="img">
                                        <div class="img-area">
                                            <img src="{{ asset('website/images/testimonials-user-1.png') }}" alt="image">
                                        </div>
                                    </div>
                                    <div class="title-area">
                                        <h6>Brice Tong</h6>
                                        <span>Santiago City</span>
                                    </div>
                                </div>
                                <div class="amount">
                                    <h6></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <p>When I hang out with my friends, we play Tournament, its so much fun</p>
                            <div class="bottom-area d-flex justify-content-between">
                                <div class="left-area d-flex">
                                    <div class="img">
                                        <div class="img-area">
                                            <img src="{{ asset('website/images/testimonials-user-1.png') }}" alt="image">
                                        </div>
                                    </div>
                                    <div class="title-area">
                                        <h6>Alva Adair</h6>
                                        <span>Santiago City</span>
                                    </div>
                                </div>
                                <div class="amount">
                                    <h6></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-item text-center">
                            <p>I joined for the community but ended up winning cash, amazing.</p>
                            <div class="bottom-area d-flex justify-content-between">
                                <div class="left-area d-flex">
                                    <div class="img">
                                        <div class="img-area">
                                            <img src="{{ asset('website/images/testimonials-user-1.png') }}" alt="image">
                                        </div>
                                    </div>
                                    <div class="title-area">
                                        <h6>Ray Sutton</h6>
                                        <span>Santiago City</span>
                                    </div>
                                </div>
                                <div class="amount">
                                    <h6></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonials In end -->

    <!-- footer-section start -->
    <footer id="footer-section">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-top">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 col-md-8">
                                    <div class="top-area text-center">
                                        <h3>JOIN VIP SUBORDINATE</h3>
                                        <p>RECEIVE CASHBACK AND SPECIAL REWARDS.</p>
                                    </div>
                                    <div class="btn-play d-flex justify-content-center align-items-center text-center">
                                        <a href="{{ route('join') }}" class="cmn-btn" style="height: 80px !important; width:300px; padding: 20px; font-size:30px;">Sign Up</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-mid pt-120">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-lg-8 col-md-8 d-flex justify-content-md-between justify-content-center align-items-center cus-grid">
                        <div class="logo-section">
                            <a class="site-logo site-title" href="{{ route('website') }}?#banner-section"><img src="{{ asset('logo.png') }}" alt="site-logo"></a>
                        </div>
                        <ul class="menu-side d-flex align-items-center">
                            <li><a href="{{ route('website') }}?#banner-section" class="active">Home</a></li>
                            <li><a href="#features-section">Features</a></li>
                            <li><a href="{{ route('contact_us') }}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 d-flex align-items-center justify-content-center justify-content-md-end">
                        <div class="right-area">
                            <ul class="d-flex">
                                <li><a href="https://www.facebook.com/groups/3056137081198675" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="{{ $link }}" target="_blank"><i class="fab fa-android"></i></a></li>
                                <li><a href="{{ $link }}" target="_blank"><i class="fab fa-apple"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="main-content">
                    <div class="row d-flex align-items-center justify-content-center">
                        <div class="col-lg-12 col-md-6">
                            <div class="left-area text-center">
                                <p>Copyright © {{ date('Y') }}. All Rights Reserved By
                                    <a href="#">Team Bulitas</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    @include('modal_disclaimer')
    <script src="{{ asset('website/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('website/js/slick.js') }}"></script>
    <script src="{{ asset('website/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('website/js/fontawesome.js') }}"></script>
    <script src="{{ asset('website/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('website/js/wow.js') }}"></script>
    <script src="{{ asset('website/js/main.js') }}"></script>
    @include('script_disclaimer')
   
</body>

</html>