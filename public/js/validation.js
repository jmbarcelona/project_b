function validation(form, err){
	let inputs = $('#'+form+' .form-control');
	inputs.each(function(index){
		let id = $(this).attr('id');
    	$("#"+id).removeClass('is-invalid');
    	let exist = document.getElementById('err_'+id);
    	let error = Object.assign({}, err);

    	if (Object.hasOwn(error, id)) {
    		let message = (err[id] !== 'undefined')? err[id] : '';
    		if (exist) {
				$('#err_'+id).text(message);
        	}else{
        		$("#"+id).parent().append('<div class="invalid-feedback" id="err_'+id+'">'+ message+'</div>');
        	}
    		$("#"+id).addClass('is-invalid');
    	}else{
    		$("#"+id).removeClass('is-invalid');
    	}
	});
}

$(document).ready(function(){
  $("form.needs-validation :input").on('input', function(){
      var _this = $(this);
      var checkClass = _this.hasClass('is-invalid');
      if (checkClass) {
        _this.removeClass('is-invalid');
      }
  });
});
