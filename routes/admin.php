<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
  

/* Borrower Routes */
Route::name('admin.')->middleware(['auth','role:admin'])->group(function () {
        Route::get('', [DashboardController::class, 'index'])->name('home');

  
        Route::group(['prefix' => 'users', 'as' => 'users.'], function(){
                Route::controller(UserController::class)->group(function () {
                        Route::get('', 'index')->name('index');
                        Route::get('subordinate-pending', 'pending')->name('subordinate.pending');
                        Route::get('vip-requests', 'listVipRequest')->name('vip.requests');
                        Route::get('superior', 'superior')->name('superior');
                        Route::get('list-superior', 'listSup')->name('list.superior');
                        Route::get('list-subordinate', 'listSub')->name('list.subordinate');
                        Route::get('list-subordinate-pending', 'listSubPending')->name('list.subordinate.pending');
                        Route::post('save/{id?}', 'save')->name('save');
                        Route::post('approve-account/{id?}', 'approveAccount')->name('account.approve');
                        Route::get('find/{id?}', 'find')->name('find');
                        Route::delete('delete/{id?}', 'delete')->name('delete');
                });
        });


});


?>