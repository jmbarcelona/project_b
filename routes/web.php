<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('website');

Route::get('/contact-us', function () {
    return view('contact_us');
})->name('contact_us');

Route::get('/login', function () {
    return view('login');
})->name('login');



/*Registration form*/
Route::get('register/{invite_id?}', [AuthController::class, 'register'])->name('join');

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function(){
    Route::controller(AuthController::class)->group(function () {
        Route::post('create-account', 'createAccount')->name('register');
        Route::post('login-account', 'loginAccount')->name('login');
        Route::get('logout-account', 'logout')->name('logout');
        Route::get('new-captcha', 'renewCaptcha')->name('new.captcha');
        Route::get('verify-account/{token?}', 'verifyAccount')->name('verify');
    });
});

Route::group(['prefix' => 'users', 'as' => 'users.'], function(){
    Route::controller(UserController::class)->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('list', 'list')->name('list');
        Route::post('save/{id?}', 'save')->name('save');
        Route::get('find/{id?}', 'find')->name('find');
        Route::delete('delete/{id?}', 'delete')->name('delete');
    });
});