<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notifications;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Providers\RouteServiceProvider;

use Validator;
use Str;
use DB;


class AuthController extends Controller
{


    public function register($invite_code = ''){
         return view('join_now', compact('invite_code'));
    }

    public function renewCaptcha(){
        return response()->json(['status' => true, 'captcha' => \Captcha::src('flat')]);
    }

    public function get_superior_by_referal_code($ref_id){
        $user = User::where('invite_code', $ref_id)->whereIn('user_type', [1, 2])->first();
        return $user->id;
    }

    public function verifyAccount($token){
        $user = User::where('forgot_token', $token)->whereNull('verified_at')->where('status', 0);
        if ($user->count() > 0) {
            $users = $user->first()->update(['status' => 2, 'verified_at' => now()]);

            if ($users) {
               return redirect(route('login'));
            }
        }else{
               return redirect(route('login'));
        }
    }

    public function loginAccount(Request $request){
        $validator = Validator::make($request->all(), [
            'email_address' => 'required|email',
            'password' => 'required',
        ]);


        if($validator->fails()){
            return response()->json(['status' => false, 'message' => 'Please check form error.', 'error' => $validator->errors() ]);
        }else{
             $credentials = ['email_address' => $request->get('email_address'), 'password' => $request->get('password')];

     
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
                $user = Auth::user();

                if (in_array($user->status, [1, 3])) {
                    if (auth()->user()->userRole() == 'admin' || auth()->user()->userRole() == 'superior') {
                       return response()->json(['status' => true, 'redirect' => route('admin.home') ]);
                    }else if (auth()->user()->userRole()== 'subordinate') {
                       return response()->json(['status' => true, 'redirect' => route('member') ]);
                    }else{
                       return response()->json(['status' => false, 'message' => auth()->user()->userRole() ]);
                    }
                }else{
                    $request->session()->invalidate();
                    $request->session()->regenerateToken();
                    Session::flush();
                    Auth::guard('web')->logout();
                    return response()->json(['status' => false, 'redirect' => route('login') ]);
                }
            }else{
                   return response()->json(['status' => false, 'message' => 'Invalid Account!']);
            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        Session::flush();
        Auth::guard('web')->logout();
        
        return redirect()->route('login');
    }

    public function createAccount(Request $request){
        $ref_id = '';
        $superior = null;
        $status = false;
        if (!empty($request->get('ref_id'))) {
          $ref_id = $request->get('ref_id');
          $superior = $this->get_superior_by_referal_code($ref_id);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5',
            'email_address' => 'required|email|unique:users,email_address',
            'lele_id' => 'required|unique:users,lele_id|min:7|max:10',
            'confirm_lele_id' => 'required|same:lele_id|min:7|max:10',
        ], ['name.min' => 'Name is too short.', 'lele_id.min' => 'Enter a valid account ID']);


        if($validator->fails()){
            return response()->json(['status' => false, 'message' => 'Please check form error.', 'error' => $validator->errors() ]);
        }else{

            $url="https://www.google.com/recaptcha/api/siteverify";
            $data=[
               'secret'=> '6LfHJvkjAAAAADSq3vfcbfxZ2HWHNhiVEigguG_Z',
               'response'=> $request->get('g-token'),
               'remoteip'=> $_SERVER['REMOTE_ADDR']
            ];
               
            $options = [
            'http' => [
              'header' => "Content-type: application/x-www-form-urlencoded\r\n",
              'method' => 'POST',
              'content' => http_build_query($data)
            ]
            ];

            $context = stream_context_create($options);
            $response = file_get_contents($url, false, $context);
            $res = json_decode($response, true);

          if ($res['success']) {
               $forgot_token = Str::random(8).Str::random(8).Str::random(8); 
               $invite_code = Str::random(8).str_replace(['=','@','/','~'], '', base64_encode($request->get('lele_id')));
               $pwd = $request->get('lele_id').Str::random(8);
            try {
                $data_email = ['name' => $request->get('name'), 'email' => $request->get('email_address'), 'password' => $pwd, 'token' => route('auth.verify', $forgot_token)];
                Mail::to($request->get('email_address'))->send(new Notifications($data_email));
                $status = true;
            } catch (Exception $e) {
                return response()->json(['status' => false, 'message' => 'Please Try again later. Make sure that your email address is active.' ]);   
            }

            if ($status) {
                   $users = User::create([
                            'email_address' => $request->get('email_address'),
                            'lele_id' => $request->get('lele_id'),
                            'password' => Hash::make($pwd),
                            'user_name' => 'USR_'.$request->get('lele_id'),
                            'referal_invite_code' => $ref_id,
                            'name' => $request->get('name'),
                            'invite_code' => 'BLTS-'.$invite_code,
                            'superior' => $superior,
                            'forgot_token' => $forgot_token,
                            'created_at' => now(),
                        ]);
                    return response()->json(['status' => true, 'message' => 'We sent you an email. Please check your email!']);
            }else{
                return response()->json(['status' => false, 'message' => 'Please Try again later. Make sure that your email address is active.' ]);
            }
            


          }else{
                return response()->json(['status' => false, 'message' => 'Captcha Expired Please try again.']);
          }

           
        }
    }
}