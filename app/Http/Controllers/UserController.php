<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;

use Illuminate\Support\Facades\Mail;
use App\Mail\Notifications;

class UserController extends Controller
{
    public function index(){
        return view('admin.subordinate');
    }
    public function superior(){
        return view('admin.superior');
    }

    public function pending(){
        return view('admin.pending');
    }

    public function listVipRequest(){
        $users = User::where('user_type', 3)->where('status', 1)->where('cashback_status', 1)->get();
        return response()->json(['status' => true, 'data' => $users ]);
    }

    public function listSub(){
        $users = User::where('user_type', 3)->where('status', 1)->get();
        return response()->json(['status' => true, 'data' => $users ]);
    }

    public function listSubPending(){
        $users = User::where('user_type', 3)->where('status', 3)->get();
        return response()->json(['status' => true, 'data' => $users ]);
    }

    public function listSup(){
        $users = User::where('user_type', 2)->get();
        return response()->json(['status' => true, 'data' => $users ]);
    }

    public function approveAccount(Request $request){
        $user = User::whereIn('user_type', [2, 3])->where('id', $request->get('id'));
        if ($user->count() > 0) {
            $user->update([
                'status' => 1,
                'verified_by' => auth()->user()->id,
                'updated_at' => now()
            ]);

            $data_email = ['name' => $user->name];
            Mail::to($user->email_address)->send(new Notifications($data_email, ''));

            return response()->json(['status' => true, 'message' => 'Account verified successfully']);
        }
    }

    public function save(Request $request, $id = ""){
        $validator = Validator::make($request->all(), [
            'lele_id' => 'required|unique:users,lele_id,'.$id,
            'email_address' => 'unique:users,email_address,'.$id,
            'contact_number' => 'unique:users,contact_number,'.$id,
            'name' => 'required',
            'status' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'error' => $validator->errors() ]);
        }else{
            if(!empty($id)){
                $users = User::where('id', $id)->update([
                    'lele_id' => $request->get('lele_id'),
                    'name' => $request->get('name'),
                    'email_address' => $request->get('email_address'),
                    'status' => $request->get('status'),
                    'gcash_name' => $request->get('gcash_name'),
                    'contact_number' => $request->get('contact_number'),
                    'user_type' => $request->get('user_type'),
                    'updated_at' => now(),
                ]);
                if($users){
                    return response()->json(['status' => true, 'message' => 'users saved successfully!']);
                }
            }else{
                $users = User::create([
                    'lele_id' => $request->get('lele_id'),
                    'name' => $request->get('name'),
                    'email_address' => $request->get('email_address'),
                    'status' => $request->get('status'),
                    'gcash_name' => $request->get('gcash_name'),
                    'contact_number' => $request->get('contact_number'),
                    'user_type' => $request->get('user_type'),
                    'created_at' => now(),
                ]);
                if($users){
                    return response()->json(['status' => true, 'message' => 'users updated successfully!']);
                }
            }
        }
    }

    public function find($id){
        $users = User::findOrFail($id);
        return response()->json(['status' => true, 'data' => $users ]);
    }

    public function delete($id){
        $users = User::findOrFail($id);
        if($users->delete()){
            return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
        }
    }

}