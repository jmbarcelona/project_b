<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class DashboardController extends Controller
{
    public function index(){
        $subordinate = User::where('user_type', 3)->where('status', 1)->count();
        $superior = User::where('user_type', 2)->where('status', 1)->count();
        $vip = User::where('user_type', 3)->where('status', 1)->where('is_vip', 1)->count();
        $new_comers = User::where('user_type', 3)->where('status', 1)->whereDate('created_at', today())->get();

        return view('admin.home', compact('subordinate','superior','vip', 'new_comers'));
    }
}