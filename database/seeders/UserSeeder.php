<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Hash;
use DB;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'lele_id' => '3160397',
            'invite_code' => 'BLTS'.str_replace(['=','@','/','~'], '', base64_encode('3160397')),
            'username' => 'johntrabulitas',
            'name' => 'JL Barcelona',
            'email_address' => 'johnluisb14@gmail.com',
            // 'password' => Hash::make('Greatwhite18!'),
            'password' => Hash::make('12345678'),
            'user_type' => 1,
            'gcash_name' => 'John Luis Barcelona',
            'contact_number' => '09754952519',
            'status' => 1,
            'verified_at' => now(),
        ]);
    }
}
