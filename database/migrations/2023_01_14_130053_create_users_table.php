<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->comment('');
            $table->integer('id', true);
            $table->string('image')->nullable();
            $table->string('invite_code')->nullable();
            $table->integer('lele_id')->nullable();
            $table->string('name')->nullable()->default('');
            $table->string('username')->nullable();
            $table->string('email_address')->nullable();
            $table->string('password')->nullable();
            $table->string('forgot_token')->nullable();
            $table->string('gcash_name')->nullable();
            $table->string('contact_number')->nullable();
            $table->integer('user_type')->nullable()->default(3)->comment('1 = superadmin, 2 = superior, 3 = member');
            $table->integer('superior')->nullable();
            $table->string('referal_invite_code')->nullable();
            $table->integer('status')->nullable()->default(0)->comment('0 = pending, 1 = approve');
            $table->integer('cashback_status')->nullable()->default(0)->comment('0 =pending, 1 = for_approve, 2 = sent');
            $table->dateTime('verified_at')->nullable();
            $table->integer('verified_by')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
