<?php
return [
    'user' => [
        'role' => [
            '1' => "admin",
            '2' => "superior",
            '3' => "subordinate",
        ]
    ],
];
